package database.dto.movie;

public enum ProjectionStatus {

    HAS_ROOM, FULL

}
