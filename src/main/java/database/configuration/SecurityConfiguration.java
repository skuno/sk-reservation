package database.configuration;

import database.token.security.CheckSecurity;
import database.token.service.TokenService;
import io.jsonwebtoken.Claims;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.IntStream;

@Aspect
@Configuration
public class SecurityConfiguration {

    private final TokenService tokenService;

    public SecurityConfiguration(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @Around("@annotation(database.token.security.CheckSecurity)")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();

        Integer bearerIndex = getBearerIndex(joinPoint, methodSignature);

        if (bearerIndex == -1)
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        String token = joinPoint.getArgs()[bearerIndex].toString().split(" ")[1];

        Claims claims = tokenService.parseToken(token);

        if (claims == null)
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        CheckSecurity checkSecurity = method.getAnnotation(CheckSecurity.class);

        String role = claims.get("role", String.class);

        if (Arrays.asList(checkSecurity.roles()).contains(role))
            return joinPoint.proceed();

        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    private Integer getBearerIndex(ProceedingJoinPoint joinPoint, MethodSignature methodSignature) {
        return IntStream.range(0, methodSignature.getParameterNames().length)
                .filter(index -> bearerFilter(joinPoint, methodSignature, index))
                .findFirst().orElse(-1);
    }

    private boolean bearerFilter(ProceedingJoinPoint joinPoint, MethodSignature methodSignature, Integer index) {
        return methodSignature.getParameterNames()[index].equals("authorization") &&
                joinPoint.getArgs()[index].toString().startsWith("Bearer");
    }

}
