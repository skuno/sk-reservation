package database.reservation.rest;

import database.reservation.domain.Reservation;
import database.reservation.service.ReservationService;
import database.token.security.CheckSecurity;
import database.util.RestUtilities;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("service/reservation")
public class ReservationController {

    private final ReservationService service;

    private final RestUtilities restUtilities;

    public ReservationController(ReservationService service, RestUtilities restUtilities) {
        this.service = service;
        this.restUtilities = restUtilities;
    }

    @PostMapping("/save")
    @CheckSecurity(roles = {"ADMIN", "USER"})
    @ApiOperation("Save a reservation or update an existing one")
    public ResponseEntity<?> saveOrUpdate(@RequestHeader("Authorization") String authorization,
                                          @Valid @RequestBody Reservation reservation, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return restUtilities.createErrorMap(bindingResult);

        if (reservation.getJwt() == null || reservation.getJwt().isEmpty())
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        return new ResponseEntity<>(service.saveOrUpdate(reservation, reservation.getJwt()), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{reservationId}")
    @CheckSecurity(roles = {"ADMIN", "USER"})
    @ApiOperation("Delete a reservation for the given reservation id")
    public ResponseEntity<?> deleteById(@RequestHeader("Authorization") String authorization,
                                        @PathVariable Long reservationId) {
        service.deleteById(reservationId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{reservationId}")
    @CheckSecurity(roles = {"ADMIN", "USER"})
    @ApiOperation("Find reservation for the given reservation id")
    public ResponseEntity<Reservation> findById(@RequestHeader("Authorization") String authorization,
                                                @PathVariable Long reservationId) {
        return service.findById(reservationId)
                .map(reservation -> new ResponseEntity<>(reservation, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/all")
    @CheckSecurity(roles = {"ADMIN", "USER"})
    @ApiOperation("Find all reservations")
    public ResponseEntity<List<Reservation>> findAll(@RequestHeader("Authorization") String authorization) {
        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }
}
