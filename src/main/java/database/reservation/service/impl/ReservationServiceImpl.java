package database.reservation.service.impl;

import database.dto.movie.Cinema;
import database.dto.movie.Projection;
import database.dto.movie.ProjectionStatus;
import database.dto.user.User;
import database.dto.user.UserStatus;
import database.exception.FullCinemaException;
import database.exception.ResourceNotFoundException;
import database.reservation.domain.Reservation;
import database.reservation.repository.ReservationRepository;
import database.reservation.service.ReservationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ReservationServiceImpl implements ReservationService {

    private final ReservationRepository repository;

    private final WebClient.Builder webClient;

    public ReservationServiceImpl(ReservationRepository repository, WebClient.Builder webClient) {
        this.repository = repository;
        this.webClient = webClient;
    }

    @Override
    public Reservation saveOrUpdate(Reservation reservation, String jwt) {
        updateProjection(reservation, jwt);
        User updatedUser = updateUser(reservation, jwt);

        Float discount = updatedUser.getUserStatus().getDiscount();

        if (discount != 0)
            reservation.setTotalPrice(reservation.getTotalPrice() - ((discount / 100) * reservation.getTotalPrice()));

        return repository.save(reservation);
    }

    private void updateProjection(Reservation reservation, String jwt) {
        Projection projection = findProjection(reservation.getProjectionId(), jwt);

        projection.setNumberOfReservations(projection.getNumberOfReservations() +
                reservation.getReservedSeats().size());

        if (!cinemaHasRoom(projection.getCinema(), projection.getNumberOfReservations()))
            throw new FullCinemaException();

        if (cinemaIsFull(projection.getCinema(), projection.getNumberOfReservations()))
            projection.setStatus(ProjectionStatus.FULL);

        webClient.build()
                .post()
                .uri("http://movie-service/service/projection/save")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .headers(httpHeaders -> httpHeaders.setBearerAuth(jwt))
                .body(BodyInserters.fromObject(projection))
                .retrieve()
                .bodyToMono(Map.class)
                .block();
    }

    private Projection findProjection(Long projectionId, String jwt) {
        return webClient.build()
                .get()
                .uri("http://movie-service/service/projection/" + projectionId)
                .headers(httpHeaders -> httpHeaders.setBearerAuth(jwt))
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals, clientResponse -> Mono.error(ResourceNotFoundException::new))
                .bodyToMono(Projection.class)
                .block();
    }

    private boolean cinemaHasRoom(Cinema cinema, int numberOfReservations) {
        return cinema.getNumberOfRows() * cinema.getNumberOfSeatsPerRow() > numberOfReservations;
    }

    private boolean cinemaIsFull(Cinema cinema, int numberOfReservations) {
        return cinema.getNumberOfRows() * cinema.getNumberOfSeatsPerRow() == numberOfReservations;
    }

    private User updateUser(Reservation reservation, String jwt) {
        User user = findUser(reservation.getUserId(), jwt);
        user.setNumberOfReservations(user.getNumberOfReservations() + 1);
        updateUserStatus(user);

        return webClient.build()
                .post()
                .uri("http://user-service/service/user/save")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(user))
                .retrieve()
                .bodyToMono(User.class)
                .block();
    }

    private User findUser(Long userId, String jwt) {
        return webClient.build()
                .get()
                .uri("http://user-service/service/user/" + userId)
                .headers(httpHeaders -> httpHeaders.setBearerAuth(jwt))
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals, clientResponse -> Mono.error(ResourceNotFoundException::new))
                .bodyToMono(User.class)
                .block();
    }

    private void updateUserStatus(User user) {
        UserStatus goldStatus = findStatusByName("GOLD");
        UserStatus diamondStatus = findStatusByName("DIAMOND");

        if (goldStatus.getRequiredPoints().equals(user.getNumberOfReservations()))
            user.setUserStatus(goldStatus);
        else if (diamondStatus.getRequiredPoints().equals(user.getNumberOfReservations()))
            user.setUserStatus(diamondStatus);
    }

    private UserStatus findStatusByName(String statusName) {
        return webClient.build()
                .get()
                .uri("http://user-service/service/userStatus/name/" + statusName)
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals, clientResponse -> Mono.error(ResourceNotFoundException::new))
                .bodyToMono(UserStatus.class)
                .block();
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<Reservation> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<Reservation> findAll() {
        return repository.findAll();
    }
}
