package database.reservation.service;

import database.reservation.domain.Reservation;

import java.util.List;
import java.util.Optional;

public interface ReservationService {

    Reservation saveOrUpdate(Reservation reservation, String jwt);

    void deleteById(Long id);

    Optional<Reservation> findById(Long id);

    List<Reservation> findAll();

}
