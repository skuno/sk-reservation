package database.reservation.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Embeddable
@Data
@NoArgsConstructor
public class ReservedSeat {

    private Integer reservedRow;

    private Integer reservedColumn;

}
