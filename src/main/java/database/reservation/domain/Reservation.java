package database.reservation.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "reservation")
@Data
@NoArgsConstructor
public class Reservation implements Serializable {

    @Id
    @Column(name = "reservation_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Positive
    private Long projectionId;

    @NotNull
    @Positive
    private Long userId;

    @NotNull
    @PositiveOrZero
    private Float totalPrice;

    @NotNull
    @ElementCollection
    private List<ReservedSeat> reservedSeats;

    @Transient
    private transient String jwt;

}
