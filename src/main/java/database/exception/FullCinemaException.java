package database.exception;

public class FullCinemaException extends RuntimeException {

    public FullCinemaException() {
        super();
    }

    public FullCinemaException(String message) {
        super(message);
    }
}
